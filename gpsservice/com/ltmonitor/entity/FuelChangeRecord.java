﻿package com.ltmonitor.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * 油量变化记录,当油量剧烈变化时记下相关的信息
 * @author DELL
 *
 */

@Entity
@Table(name="FuelChangeRecord")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)  
public class FuelChangeRecord extends TenantEntity
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "enclosureId", unique = true, nullable = false)
	private int entityId;
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}
	private String plateNo;
	public final String getPlateNo()
	{
		return plateNo;
	}
	public final void setPlateNo(String value)
	{
		plateNo = value;
	}

	//变化值
	private double fuel;
	public final double getFuel()
	{
		return fuel;
	}
	public final void setFuel(double value)
	{
		fuel = value;
	}

	//add 增加, reduce 减少
	private String type;
	public final String getType()
	{
		return type;
	}
	public final void setType(String value)
	{
		type = value;
	}
	//发生时间
	private java.util.Date happenTime = new java.util.Date(0);
	public final java.util.Date getHappenTime()
	{
		return happenTime;
	}
	public final void setHappenTime(java.util.Date value)
	{
		happenTime = value;
	}

	private double latitude;
	public final double getLatitude()
	{
		return latitude;
	}
	public final void setLatitude(double value)
	{
		latitude = value;
	}

	private double longitude;
	public final double getLongitude()
	{
		return longitude;
	}
	public final void setLongitude(double value)
	{
		longitude = value;
	}

	private double mileage;
	public final double getMileage()
	{
		return mileage;
	}
	public final void setMileage(double value)
	{
		mileage = value;
	}

	//地理位置
	private String location;
	public final String getLocation()
	{
		return location;
	}
	public final void setLocation(String value)
	{
		location = value;
	}

	//手工添加
	private String manual;
	public final String getManual()
	{
		return manual;
	}
	public final void setManual(String value)
	{
		manual = value;
	}

	public FuelChangeRecord()
	{
		setCreateDate(new java.util.Date());
	}


}