package com.ltmonitor.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "driverInfo")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class DriverInfo extends TenantEntity {
	private static final long serialVersionUID = -4955293596092558178L;
	private String companyNo;
	/**
	 * 驾驶车辆
	 */
	private int vehicleId;
	private String driverCode = "";
	private String driverName;
	private String sex;
	/**
	 * 从业资格证，驾驶证
	 */
	private String driverLicence = "";
	/**
	 * 身份证
	 */
	private String identityCard = "";

	private String nativePlace = "";
	private String address = "";
	/**
	 * 联系电话
	 */
	private String telephone = "";
	private String mobilePhone = "";
	private Date birthday = new Date();
	private String drivingType = "";
	private Date examineYear = new Date();
	private Short harnessesAge = 0;
	private Short status = 0;
	private Date appointment = new Date();
	//private Float baseSalary = Float.valueOf(0.0F);
	//private Float royaltiesScale = Float.valueOf(0.0F);
	private Float appraisalIntegral = Float.valueOf(0.0F);
	private String driverRfid = "";
	private String password = "";
	private Integer operatorId;
	private Date register = new Date();
	/**
	 * 发证机构
	 */
	private String licenseAgency;
	private Date updateTime = new Date();

	private Date certificationDate = new Date();

	private Date invalidDate = new Date();

	private String corp = "";
	/**
	 * 监管机构
	 */
	private String monitorOrg = "";

	private String monitorPhone = "";

	private Integer serviceLevel = Integer.valueOf(3);

	private String bgTitle = "";

	private String location = "";
	private Integer photoFormat;

	@Transient
	private Integer checkCode;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "driverId", unique = true, nullable = false)
	private int entityId;

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int value) {
		entityId = value;
	}

	@Column(name = "CompanyNo", nullable = false, length = 20)
	public String getCompanyNo() {
		return this.companyNo;
	}

	public void setCompanyNo(String companyNo) {
		this.companyNo = companyNo;
	}

	@Column(name = "driverCode", unique = true, nullable = false, length = 8)
	public String getDriverCode() {
		return this.driverCode;
	}

	public void setDriverCode(String driverCode) {
		this.driverCode = driverCode;
	}

	@Column(name = "driverName", nullable = false, length = 32)
	public String getDriverName() {
		return this.driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	@Column(name = "Sex", nullable = false, length = 8)
	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	@Column(name = "driverLicence", nullable = false, length = 20)
	public String getDriverLicence() {
		return this.driverLicence;
	}

	public void setDriverLicence(String driverLicence) {
		this.driverLicence = driverLicence;
	}

	@Column(name = "IdentityCard", nullable = false, length = 20)
	public String getIdentityCard() {
		return this.identityCard;
	}

	public void setIdentityCard(String identityCard) {
		this.identityCard = identityCard;
	}

	@Column(name = "NativePlace", nullable = false, length = 50)
	public String getNativePlace() {
		return this.nativePlace;
	}

	public void setNativePlace(String nativePlace) {
		this.nativePlace = nativePlace;
	}

	@Column(name = "Address", nullable = false, length = 64)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "Telephone", nullable = false, length = 32)
	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Column(name = "mobilePhone", nullable = false, length = 32)
	public String getMobilePhone() {
		return this.mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	@Column(name = "Birthday", length = 23)
	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@Column(name = "DrivingType", nullable = false, length = 8)
	public String getDrivingType() {
		return this.drivingType;
	}

	public void setDrivingType(String drivingType) {
		this.drivingType = drivingType;
	}

	@Column(name = "ExamineYear", length = 23)
	public Date getExamineYear() {
		return this.examineYear;
	}

	public void setExamineYear(Date examineYear) {
		this.examineYear = examineYear;
	}

	@Column(name = "HarnessesAge", nullable = false)
	public Short getHarnessesAge() {
		return this.harnessesAge;
	}

	public void setHarnessesAge(Short harnessesAge) {
		this.harnessesAge = harnessesAge;
	}

	@Column(name = "Status", nullable = false)
	public Short getStatus() {
		return this.status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	@Column(name = "Appointment")
	public Date getAppointment() {
		return this.appointment;
	}

	public void setAppointment(Date appointment) {
		this.appointment = appointment;
	}

	@Column(name = "AppraisalIntegral", nullable = false, precision = 24, scale = 0)
	public Float getAppraisalIntegral() {
		return this.appraisalIntegral;
	}

	public void setAppraisalIntegral(Float appraisalIntegral) {
		this.appraisalIntegral = appraisalIntegral;
	}

	@Column(name = "DriverRFID", nullable = false, length = 40)
	public String getDriverRfid() {
		return this.driverRfid;
	}

	public void setDriverRfid(String driverRfid) {
		this.driverRfid = driverRfid;
	}

	@Column(name = "Password", nullable = false, length = 16)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getOperatorId() {
		return this.operatorId;
	}

	public void setOperatorId(Integer operatorId) {
		this.operatorId = operatorId;
	}

	public Date getRegister() {
		return this.register;
	}

	public void setRegister(Date register) {
		this.register = register;
	}


	public String getLicenseAgency() {
		return this.licenseAgency;
	}

	public void setLicenseAgency(String licenseAgency) {
		this.licenseAgency = licenseAgency;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getCorp() {
		return this.corp;
	}

	public void setCorp(String corp) {
		this.corp = corp;
	}

	public String getMonitorOrg() {
		return this.monitorOrg;
	}

	public void setMonitorOrg(String monitorOrg) {
		this.monitorOrg = monitorOrg;
	}

	public String getMonitorPhone() {
		return this.monitorPhone;
	}

	public void setMonitorPhone(String monitorPhone) {
		this.monitorPhone = monitorPhone;
	}

	public Integer getServiceLevel() {
		return this.serviceLevel;
	}

	public void setServiceLevel(Integer serviceLevel) {
		this.serviceLevel = serviceLevel;
	}

	public String getBgTitle() {
		return this.bgTitle;
	}

	public void setBgTitle(String bgTitle) {
		this.bgTitle = bgTitle;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getPhotoFormat() {
		return this.photoFormat;
	}

	public void setPhotoFormat(Integer photoFormat) {
		this.photoFormat = photoFormat;
	}

	public Date getCertificationDate() {
		return this.certificationDate;
	}

	public void setCertificationDate(Date certificationDate) {
		this.certificationDate = certificationDate;
	}

	public Date getInvalidDate() {
		return this.invalidDate;
	}

	public void setInvalidDate(Date invalidDate) {
		this.invalidDate = invalidDate;
	}


	@Transient
	public Integer getCheckCode() {
		return this.checkCode;
	}

	public void setCheckCode(Integer checkCode) {
		this.checkCode = checkCode;
	}

	public int getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}
}

/*
 * Location: E:\goss_web\goss_service\goss_service.jar Qualified Name:
 * com.yuweitek.goss.model.DriverInfo JD-Core Version: 0.6.0
 */