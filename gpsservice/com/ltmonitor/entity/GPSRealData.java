﻿package com.ltmonitor.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;

//GPS实时数据

@Entity
@Table(name = "GPSRealData")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class GPSRealData implements Serializable {
	// 唯一库表ID，没有实际意义
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int iD;

	public final int getID() {
		return iD;
	}

	public final void setID(int value) {
		iD = value;
	}

	public GPSRealData() {
		alarmState = "00000000000000000000000000000000";
		status = "00000000000000000000000000000000";

		setSendTime(new java.util.Date());
	}

	private int vehicleId;
	// 设备终端状态
	private String status;

	public final String getStatus() {
		return status;
	}

	public final void setStatus(String value) {
		status = value;
	}

	// 部门
	private int depId;
	// 部门名称
	@Transient
	private String depName;

	// 电路状态,也就是报警状态
	private String alarmState;

	public final String getAlarmState() {
		return alarmState;
	}

	public final void setAlarmState(String value) {
		alarmState = value;
	}

	// 车牌号
	private String plateNo;

	public final String getPlateNo() {
		return plateNo;
	}

	public final void setPlateNo(String value) {
		plateNo = value;
	}

	// 终端围栏报警的区域ID
	@Transient
	private int enclosureId;
	// 围栏报警类型 0：进； 1：出
	@Transient
	private int enclosureAlarm;

	// 地理位置的文字描述,如省,市，县，路的详细描述
	private String location;

	public final String getLocation() {
		return location;
	}

	public final void setLocation(String value) {
		location = value;
	}

	// 车终端卡号
	private String simNo;

	public final String getSimNo() {
		return simNo;
	}

	public final void setSimNo(String value) {
		simNo = value;
	}

	// 发送时间
	private java.util.Date sendTime = new java.util.Date(0);

	public final java.util.Date getSendTime() {
		return sendTime;
	}

	public final void setSendTime(java.util.Date value) {
		sendTime = value;
	}

	// 入库时间
	private java.util.Date updateDate = new java.util.Date(0);

	public final java.util.Date getUpdateDate() {
		return updateDate;
	}

	public final void setUpdateDate(java.util.Date value) {
		updateDate = value;
	}

	// 经度
	private double longitude;

	public final double getLongitude() {
		return longitude;
	}

	public final void setLongitude(double value) {
		longitude = value;
	}

	// 纬度
	private double latitude;

	public final double getLatitude() {
		return latitude;
	}

	public final void setLatitude(double value) {
		latitude = value;
	}

	// 速度
	private double velocity;

	public final double getVelocity() {
		return velocity;
	}

	public final void setVelocity(double value) {
		velocity = value;
	}

	// 方向
	private int direction;

	public final int getDirection() {
		return direction;
	}

	public final void setDirection(int value) {
		direction = value;
	}

	// 海拔
	private double altitude;
	
	private int enclosureType;
	
	// 行驶记录仪速度
	private double recordVelocity;

	public final double getRecordVelocity() {
		return recordVelocity;
	}

	public final void setRecordVelocity(double value) {
		recordVelocity = value;
	}

	// 行驶里程总量
	private double mileage;

	public final double getMileage() {
		return mileage;
	}

	public final void setMileage(double value) {
		mileage = value;
	}

	// 当前油量
	private double gas;

	// GPS的定位状态，false代表没有定位,被屏蔽或找不到卫星
	private boolean valid;

	public final boolean IsValid() {
		return valid;
	}

	public final void setValid(boolean value) {
		valid = value;
	}

	// 视频在线状态
	private String dvrStatus;

	public final String getDvrStatus() {
		return dvrStatus;
	}

	public final void setDvrStatus(String value) {
		dvrStatus = value;
	}

	// GPS设备在线状态, false代表不在线
	private boolean online;

	public final boolean getOnline() {
		return online;
	}

	public final void setOnline(boolean value) {
		online = value;
	}

	public double getGas() {
		return gas;
	}

	public void setGas(double gas) {
		this.gas = gas;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	public int getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getDepName() {
		return depName;
	}

	public void setDepName(String depName) {
		this.depName = depName;
	}

	public int getDepId() {
		return depId;
	}

	public void setDepId(int depId) {
		this.depId = depId;
	}

	public int getEnclosureId() {
		return enclosureId;
	}

	public void setEnclosureId(int enclosureId) {
		this.enclosureId = enclosureId;
	}

	public int getEnclosureAlarm() {
		return enclosureAlarm;
	}

	public void setEnclosureAlarm(int enclosureAlarm) {
		this.enclosureAlarm = enclosureAlarm;
	}

	public int getEnclosureType() {
		return enclosureType;
	}

	public void setEnclosureType(int enclosureType) {
		this.enclosureType = enclosureType;
	}

}