package com.ltmonitor.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * �ϴ��ı������ ���·��ı���������Ϣ ����
 * 
 * @author DELL
 * 
 */
public class WarnData  implements Serializable {

	public static int OVER_SPEED = 0x0001;// ���ٱ���
	public static int TIRED = 0x0002;// ƣ�ͼ�ʻ����
	public static int EMERGENCY = 0x0003;// ��������
	public static int IN_AREA = 0x0004;// ����ָ�����򱨾�
	public static int OUT_AREA = 0x0005;// �뿪ָ�����򱨾�
	public static int JAM = 0x0006;// ·�ζ���
	public static int RISK_ROUTE = 0x0007;// Σ��·�α���
	public static int CROSS_BORDER = 0x0008;// Խ�籨��
	public static int STEAL = 0x0009;// ����
	public static int ROB = 0x000A;// �پ�

	public static int OFFSET_ROUTE = 0x000B;// ƫ��·�߱���
	public static int MOVE = 0x000C;// �����ƶ�����
	public static int OVER_TIME = 0x000D;// ��ʱ��ʻ����
	public static int OTHER = 0x000E;// ����
	
	public static int FROM_TERMINAL = 0x01;//�����ն�
	public static int FROM_PLATFROM = 0x02;//��ҵ���ƽ̨
	public static int FROM_GOV=0x03;//�������ƽ̨
	public static int FROM_OTHER = 0x09;//����


	// ���ƺ�
	private String plateNo;
	private int plateColor;
	// ������Ϣ��Դ
	private int src;
	// ��������
	private int type;
	// ����ʱ��
	private Date warnTime;
	// ��������Id
	private long infoId;
	// ��������
	private String content;
	// ��������ʱ��
	private Date supervisionEndTime;
	// ���켶�� 0 ���� 1 һ��
	private byte supervisionLevel;
	// ����������
	private String supervisor;
	// �����˵绰
	private String supervisionTel;
	// ����������
	private String supervisionEmail;
	// ����������
	private int result;

	public int getSrc() {
		return src;
	}

	public void setSrc(int src) {
		this.src = src;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Date getWarnTime() {
		return warnTime;
	}

	public void setWarnTime(Date warnTime) {
		this.warnTime = warnTime;
	}

	public long getInfoId() {
		return infoId;
	}

	public void setInfoId(long infoId) {
		this.infoId = infoId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getPlateColor() {
		return plateColor;
	}

	public void setPlateColor(int plateColor) {
		this.plateColor = plateColor;
	}

	public Date getSupervisionEndTime() {
		return supervisionEndTime;
	}

	public void setSupervisionEndTime(Date supervisionEndTime) {
		this.supervisionEndTime = supervisionEndTime;
	}

	public byte getSupervisionLevel() {
		return supervisionLevel;
	}

	public void setSupervisionLevel(byte supervisionLevel) {
		this.supervisionLevel = supervisionLevel;
	}

	public String getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public String getSupervisionTel() {
		return supervisionTel;
	}

	public void setSupervisionTel(String supervisionTel) {
		this.supervisionTel = supervisionTel;
	}

	public String getSupervisionEmail() {
		return supervisionEmail;
	}

	public void setSupervisionEmail(String supervisionEmail) {
		this.supervisionEmail = supervisionEmail;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getResult() {
		return result;
	}

}
