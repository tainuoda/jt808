package com.ltmonitor.util;

public class ClassInstantiateException extends RuntimeException{
	
	
	public ClassInstantiateException(Throwable e){
		super(e);
	}
	

	public ClassInstantiateException(String e){
		super(e);
	}

}
