package com.ltmonitor.mongo;

import java.net.UnknownHostException;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

public class MongoTemplate {
	public static DB getConnect(String databaseName) {
		Mongo mongo = null;
		try {
			mongo = new Mongo("192.168.110.242", 27017);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		DB db = mongo.getDB(databaseName);
		// DBCollection userCol = db.getCollection("gps");
		return db;
	}

	public DBObject findDocumentById(DBCollection collection, String id) {
		BasicDBObject query = new BasicDBObject();
		query.put("_id", new ObjectId(id));
		DBObject dbObj = collection.findOne(query);
		return dbObj;
	}

	public static void main(String[] args) {
		MongoTemplate.getConnect("erp");
	}

}
