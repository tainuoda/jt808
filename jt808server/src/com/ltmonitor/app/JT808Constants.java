﻿package com.ltmonitor.app;

public class JT808Constants
{
	public static java.util.Hashtable CommandDescr = new java.util.Hashtable();
	private static void loadMap()
	{
		CommandDescr.put("0x8804", "录音命令");
		CommandDescr.put("0x8201", "点名");
		CommandDescr.put("0x8600", "设置圆形区域");
		CommandDescr.put("0x8602", "设置矩形区域");
		CommandDescr.put("0x8604", "设置多边形区域");
		CommandDescr.put("0x8606", "设置线路");
		CommandDescr.put("0x8802", "媒体检索");
		CommandDescr.put("0x8803", "媒体上传");
		CommandDescr.put("0x8805", "单条存储多媒体上传命令");
		CommandDescr.put("0x8202", "位置跟踪");
		CommandDescr.put("0x8300", "文本信息下发");
		CommandDescr.put("0x8103", "设置终端参数");
		CommandDescr.put("0x8104", "查询终端参数");
		CommandDescr.put("0x8105", "终端控制");
		CommandDescr.put("0x8801", "拍照");
		CommandDescr.put("0x8301", "设置事件");
		CommandDescr.put("0x8302", "提问下发");
		CommandDescr.put("0x8303", "菜单设置");
		CommandDescr.put("0x8304", "信息服务");
		CommandDescr.put("0x8400", "电话回拨");
		CommandDescr.put("0x8401", "电话本设置");
		CommandDescr.put("0x8500", "车辆控制");
		CommandDescr.put("0x8700", "行车记录仪采集");
		CommandDescr.put("0x8701", "行车记录参数下传命令");


		CommandDescr.put("0x0001", "终端应答");
		CommandDescr.put("0x0102", "终端鉴权");
		CommandDescr.put("0x0100", "终端注册");
		CommandDescr.put("0x0104", "查询终端参数应答");
		CommandDescr.put("0x0201", "点名应答");
	}

	public static String GetDescr(String cmdType)
	{
		if(CommandDescr.isEmpty())
		{
			loadMap();
		}
		return "" + CommandDescr.get(cmdType);
	}

	//录音命令
	public static final int CMD_AUDIO_RECORDER = 0x8804;

	//点名
	public static final int CMD_REAL_MONITOR = 0x8201;

	//设置圆形区域
	public static final int CMD_CIRCLE_CONFIG = 0x8600;

	//设置矩形区域
	public static final int CMD_RECT_CONFIG = 0x8602;

	//设置多边形区域
	public static final int CMD_POLYGON_CONFIG = 0x8604;

	//设置线路
	public static final int CMD_ROUTE_CONFIG = 0x8606;

	//媒体检索
	public static final int CMD_MEDIA_SEARCH = 0x8802;

	//媒体上传
	public static final int CMD_MEDIA_UPLOAD = 0x8803;

	//单条存储多媒体上传命令
	public static final int CMD_MEDIA_UPLOAD_SINGLE = 0x8805;

	//位置跟踪
	public static final int CMD_LOCATION_MONITOR = 0x8202;

	//文本信息下发
	public static final int CMD_SEND_TEXT = 0x8300;

	//设置终端参数
	public static final int CMD_CONFIG_PARAM = 0x8103;

	//查询终端参数
	public static final int CMD_QUERY_PARAM = 0x8104;

	//终端控制
	public static final int CMD_CONTROL_TERMINAL = 0x8105;

	//拍照
	public static final int CMD_TAKE_PHOTO = 0x8801;

	//设置事件
	public static final int CMD_EVENT_SET = 0x8301;
	//提问下发
	public static final int CMD_QUESTION = 0x8302;
	//菜单设置
	public static final int CMD_SET_MENU = 0x8303;
	//信息服务
	public static final int CMD_INFORMATION = 0x8304;

	//电话回拨
	public static final int CMD_DIAL_BACK = 0x8400;
	//电话本设置
	public static final int CMD_PHONE_BOOK = 0x8401;
	//车辆控制
	public static final int CMD_CONTROL_VEHICLE = 0x8500;

	//行车记录仪采集
	public static final int CMD_VEHICLE_RECORDER = 0x8700;

	//行车记录参数下传命令
	public static final int CMD_VEHICLE_RECORDER_CONFIG = 0x8701;

}