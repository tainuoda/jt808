﻿package com.ltmonitor.jt808.service;

import com.ltmonitor.entity.GPSRealData;
import com.ltmonitor.entity.VehicleData;
import com.ltmonitor.jt808.protocol.T808Message;


public interface IGpsDataService
{
	void SaveRealData(GPSRealData rd);
	//得到实时数据
	// GPSRealData GetRealData(string PlateNo);
	GPSRealData getRealDataBySimNo(String VehicleId);
	//得到所有的GPS实时数据
	//java.util.List GetAllGpsRealData();

	void ProcessMessage(T808Message message);

	//void CreateStopRecord(VehicleInfo vi, GPSRealData rd);

	//得到车辆数据
	VehicleData GetVehicleBySimNo(String VehicleId);
	//VehicleData GetVehicle(string PlateNo);
	//重置GPS上线状态
	void resetGpsOnlineState();

}